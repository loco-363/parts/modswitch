# ***********************************************
# ***    Loco363 - Parts - Modular Switch     ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import math

import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class ModSwitch(generic.bases.View):
	def __init__(self, c = None, apos = 8, name = None, bpos = 0, pos = range(8), dpos = 0, *, handle = True, hole = 12, locks = None, label = None, label_both = False, pos_circle = False):
		'''Constructor
		
		@param c - center Coords
		@param apos - number of all available positions (for specific version of ModSwitch)
			Currently supports 4, 8 and 12 positions.
		@param name - placed part label
		@param bpos - base position
		@param pos - positions' labels (use None to skip specific position)
		@param dpos - the default position (the handle will be drawn with solid lines in it)
		@param handle - whether to draw the switch handle
		@param hole - diameter in label for handle axis
			Used when label = True and handle = False or when there is any handle lock.
		@param locks - position numbers to draw the handle locks in
			Used when label = True.
			Drawn as dashed when handle = True.
		@param label - whether to draw a label with description
			True/False = for direct setting
			None = draw it when there is a name specified
		@param label_both
			- False = draw name and positions' labels (middle anchored) into single (middle) circle
			- True (for colliding texts) = draw name into the inner circle, whereas positions' labels (anchored to the outer edge of label) into the outer one
		@param pos_circle
			- False = draw it on single line
			- True = whether to draw the positions' labels onto circle
		'''
		
		super().__init__(c)
		c = self.getCoords()
		
		# check arguments
		apos = int(apos)
		if apos not in (4, 8, 12):
			raise ValueError('Supported number of positions are 4, 8 or 12!')
		
		bpos = int(bpos) % apos
		
		if len(pos) < 1 or len(pos) > apos:
			raise ValueError('There could be from 1 to '+str(apos)+' positions!')
		
		dpos = int(dpos) % apos
		
		# font size of label texts
		self._font_size = '5.65mm'
		
		# optional label
		if label or (label == None and name != None and name != ''):
			label = True
			
			# label body
			self.add(gen_draw.shapes.Circle(
				c,
				35,
				properties={
					'fill': 'none',
					'stroke': 'black',
					'stroke-width': 3
				}
			))
			
			# handle locks
			if locks and len(locks) > 0:
				props = {
					'fill': 'none',
					'stroke': 'black',
					'stroke-linecap': 'round',
					'stroke-linejoin': 'round',
					'stroke-width': '0.3mm'
				}
				
				if handle:
					props['stroke-dasharray'] = '1,3'
					props['stroke-linecap'] = 'square'
					props['stroke-linejoin'] = 'square'
				
				r = hole/2
				
				# hole - better will be to draw everything using single path
				self.add(gen_draw.shapes.Circle(
					c,
					r,
					properties=props
				))
				
				lw = 5
				lh = 4
				
				x = math.sqrt(r**2 - (lw/2)**2)
				
				for l in locks:
					l = l % apos
					
					self.add(gen_draw.shapes.Polygon(
						C(c, -(360 / apos * l)),
						(
							(-lw/2, x),
							(-lw/2, x+lh),
							( lw/2, x+lh),
							( lw/2, x)
						),
						properties=props
					))
			elif not handle:
				# label hole without locks
				self.add(gen_draw.shapes.Circle(
					c,
					hole/2,
					properties={
						'fill': 'none',
						'stroke': 'black',
						'stroke-width': '0.3mm'
					}
				))
		
		# optional name
		if name != None and name != '':
			#TODO text on path arc
			self.add(
				gen_draw.shapes.Text(
					C(c, (0, (-23.5 if label_both else -27))),
					'',
					properties={
						'text-anchor': 'middle',
						'fill': 'black',
						'style': 'font-size:'+self._font_size+'; font-weight:bold;'
					}
				)
				.addLine(name, '0.5em')
			)
		
		# additional drawings
		if handle:
			handles = gen_draw.Drawing(c)
			self.add(handles)
		
		if label:
			pos_labels = gen_draw.Drawing(c)
			self.add(pos_labels)
		
		# positions
		for i in range(len(pos)):
			if pos[i] != None:
				n = (bpos + i) % apos
				
				if handle:
					if n == dpos:
						handles.addAfter(self._createHandle(360 / apos * n))
					else:
						handles.add(self._createHandle(360 / apos * n, True))
				
				if label:
					pos_labels.add(self._createPosLabel(apos, n, (30.5 if label_both else 27), pos[i], pos_circle))
	# constructor
	
	def _createHandle(self, angle, alt = False):
		'''Creates drawing of handle
		
		@param angle - angle of the handle
		@param alt - if True, draw the handle as alternative switch position (dashed)
		@return handle drawing
		'''
		
		c = self.getCoords()
		c = C(c, -angle)# we are using negative angle for counter-clockwise, but the Coords transformations uses right-hand-rule angles
		
		d = gen_draw.Drawing(c)
		
		# shapes properties
		props = {
			'fill': 'none',
			'stroke': '#555' if alt else '#000',
			'stroke-linecap': 'round',
			'stroke-linejoin': 'round',
			'stroke-width': 0.5 if alt else 2
		}
		
		if alt:
			props['stroke-dasharray'] = '1,3'
		
		# body
		d.add(gen_draw.shapes.Circle(
			c,
			20,
			properties=props
		))
		
		# bar
		d.add(gen_draw.shapes.Polygon(
			c,
			(
				(-8, -24),
				( 8, -24),
				( 5,   5),
				( 4,  20),
				( 0,  24),
				(-4,  20),
				(-5,   5)
			),
			properties=props
		))
		
		# screw
		d.add(gen_draw.shapes.Circle(
			c,
			2.5,
			properties=props
		))
		
		return d
	# _createHandle
	
	def _createPosLabel(self, apos, n, r, label, circle = False):
		'''Creates switch position's label
		
		@param apos - number of all available positions
		@param n - position number
		@param r - radius to place label center on
		@param label - position label
		@param circle - if True, draw the label onto circle
		'''
		
		c = self.getCoords()
		
		# compute position
		xy = C(None, (0,0))
		xy = C(xy, -(360 / apos * n))
		xy = C(xy, (0, r))
		xy = xy.getAbsolute()
		
		#TODO draw text on path for circle=True
		t = gen_draw.shapes.Text(
			C(c, xy),
			'',
			properties={
				'text-anchor': 'middle',
				'fill': 'black',
				'style': 'font-size:'+self._font_size+'; font-weight:bold;'
			}
		)
		
		# multi-line
		label = str(label).split('\n')
		
		# compute position of first line to vertically center whole text at given coordinates
		t.addLine(label[0], str(1-len(label)/2 - 0.15)+'em')# 0.15 is a correction as the center of characters are not at the center of line
		
		# add additional lines
		for i in range(1, len(label)):
			t.addLine(label[i])
		
		return t
	# _createPosLabel
# class ModSwitch


class ModSwitch4(ModSwitch):
	def __init__(self, c = None, *args, **kwargs):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 4, *args, **kwargs)
	# constructor
# class ModSwitch4


class ModSwitch8(ModSwitch):
	def __init__(self, c = None, *args, **kwargs):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 8, *args, **kwargs)
	# constructor
# class ModSwitch8


class ModSwitch12(ModSwitch):
	def __init__(self, c = None, *args, **kwargs):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 12, *args, **kwargs)
	# constructor
# class ModSwitch12


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(ModSwitch(), True))
