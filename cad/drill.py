# ***********************************************
# ***    Loco363 - Parts - Modular Switch     ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class ModSwitch(generic.bases.Drill):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outer-circle
		self.add(gen_draw.shapes.Circle(
			c,
			35,
			properties={
				'fill': self.COLOR_UNI
			}
		))
		
		# inner-circle
		self.add(gen_draw.shapes.Circle(
			c,
			10,
			properties={
				'fill': 'white'
			}
		))
		
		# coords
		self.add(generic.CoordsAuto(c, pos=(0, -5, 'middle')))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, ( 20,  20)), 4, 'Z', cpos=(0, -6, 'middle')))
		self.add(generic.DrillHole(C(c, (-20,  20)), 4, 'Z', cpos=(0, -6, 'middle')))
		self.add(generic.DrillHole(C(c, ( 20, -20)), 4, 'Z', cpos=(0, -6, 'middle')))
		self.add(generic.DrillHole(C(c, (-20, -20)), 4, 'Z', cpos=(0, -6, 'middle')))
	# constructor
# class ModSwitch


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(ModSwitch(), True))
